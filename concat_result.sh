#!/bin/bash

resultdir=resultat

if [ "$1" == "" ]
then
    echo "Paramètre manquat : RCR"
    exit 1
fi
rcr=$1

if [ -f $resultdir/metadata$rcr.tsv ]
then
    echo "Le fichier de métadonnées metadata$rcr.tsv existe"
else
    echo "Le fichier de métadonnées metadata$rcr.tsv n'existe pas"
    exit 1
fi

if [ `ls -1 $resultdir/data$rcr* 2>/dev/null | wc -l | tr -d $' '` -eq 0 ]
then
    echo "Aucun fichier de données"
    exit 1
else
    echo "`ls -1 $resultdir/data$rcr* 2>/dev/null | wc -l | tr -d $' '` fichier(s) de données existe(nt)"
fi

declare -i nbnoticesattendues
declare -i nbnoticesobtenues
nbnoticesobtenues=0
nbnoticesattendues=0

nbnoticesattendues=`grep "Nb notices" $resultdir/metadata$rcr.tsv | cut -f2 |  tr -d ' ' | tr -d $'\r'`
nbnoticesobtenues=`cat $resultdir/data$rcr* | sort | uniq | wc -l | tr -d ' '`

if [ "$nbnoticesattendues" -eq  "$nbnoticesobtenues" ]
then
    echo "Nombre de notices correct : $nbnoticesobtenues"
else
    echo "Nombre de notices erronné : $nbnoticesattendues attendues, $nbnoticesobtenues trouvées"
    exit 1
fi

echo "Création de $resultdir/totaldata$rcr.tsv"
cat $resultdir/data$rcr* | sort | uniq > $resultdir/totaldata$rcr.tsv
echo "Suppression des fichiers temporaires"
rm $resultdir/data$rcr* 
