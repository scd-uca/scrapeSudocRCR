import sys
from bs4 import BeautifulSoup
import re
import csv
import requests
import time

# SCRIPT EN PYTHON3
# Recherche les notices d'un RCR donné
# Sortie : 1 fichier pour chaque lot de 1000 notices, nommé data[RCR]_[debut]-[fin].tsv, avec [RCR] le numéro RCR de la bibliothèque, [debut] la 1re notice, [fin] la dernière
# Chaque fichier comprend 3 champs séparés par des tabulations :
# - PPN
# - titre (et éventuellement type de support)
# 1er auteur / édition (le cas échéant) / éditeur / date
# Ex :
# 015260321 | An introduction to acoustics [Texte imprimé] | Randall, Robert H. (1903-1983) / Addison-Wesley / cop. 1951
# 192044478	Thermodynamics : an introductory text for engineering students	Lee, John Francis (1918-....) / second edition / Addison-Wesley / cop. 1963

# Pour changer de RCR, modifier la variable globale rcr
# Utiliser les variables debut et fin pour faire une récupération partielle


# délai entre chaque requête en sec
delai=0.5
delailong=5
setparam="1"
ttlparam="1"
destdossier="resultat"
debut=1
final=0
nbnoticesparfichier=1000
new_iter=0

liste_rcr = {
    "060889901" : {"nom" : "Bib électronique", "perimetre" : "SCD", "aleph": "non"},
    "060882102" : {"nom" : "BU Médecine", "perimetre" : "SCD", "aleph": "oui"},
    "060882236" : {"nom" : "BU STAPS", "perimetre" : "SCD", "aleph": "oui"},
    "060882105" : {"nom" : "BU Saint-Jean d'Angély", "perimetre" : "SCD", "aleph": "oui"},
    "060882104" : {"nom" : "BU LASH", "perimetre" : "SCD", "aleph": "oui"},
    "060882101" : {"nom" : "BU Sciences", "perimetre" : "SCD", "aleph": "oui"},
    "060882103" : {"nom" : "BU Droit et Science politique", "perimetre" : "SCD", "aleph": "oui"},
    "061522101" : {"nom" : "Learning Centre SophiaTech", "perimetre" : "SCD", "aleph": "oui"},
    "060882232" : {"nom" : "OCA (Laboratoire J.-L. Lagrange)", "perimetre" : "hors SCD", "aleph": "oui"},
    "061522202" : {"nom" : "OCA (Laboratoire Géoazur)", "perimetre" : "hors SCD", "aleph": "oui"},
    "060885209" : {"nom" : "OCA", "perimetre" : "hors SCD", "aleph": "oui"},
    "060882233" : {"nom" : "IUT (Nice)", "perimetre" : "hors SCD", "aleph": "oui"},
    "060832201" : {"nom" : "IUT (Menton)", "perimetre" : "hors SCD", "aleph": "oui"},
    "060292201" : {"nom" : "IUT (Cannes)", "perimetre" : "hors SCD", "aleph": "oui"},
    "060882202" : {"nom" : "LJAD", "perimetre" : "hors SCD", "aleph": "oui"},
    "060882238" : {"nom" : "Service Commun en Langues", "perimetre" : "hors SCD", "aleph": "oui"},
    "060882304" : {"nom" : "Villa Arson", "perimetre" : "hors SCD", "aleph": "non"}
}

def recherche_sudoc (i,j):
    results=[]
    session = requests.Session()
    session.headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8','Referer': 'http://www.sudoc.abes.fr/xslt/DB=2.1/SET=1/TTL=21//CMD?ACT=SRCHA&IKT=1016&SRT=RLV&TRM=rbc+061522101&COOKIE=U10178,Klecteurweb,I250,B341720009+,SY,NLECTEUR+WEBOPC,D2.1,Ebc3613e0-1,A,H,R176.159.79.166,FY' }
    base_url="http://www.sudoc.abes.fr/xslt"
    base_params="//DB=2.1/SET="+setparam+"/TTL="+ttlparam+"/CMD?"
    params="ACT=SRCHA&IKT=1016&SRT=RLV&TRM=rbc+"+rcr
    url=base_url+base_params+params
    print ("URL : "+url)
    err=""
    try:
        r1 = session.get(url,timeout=(7, 27),allow_redirects=False)
        if r1.status_code == 302: # expected here
            jar = r1.cookies
            redirect_URL1 = r1.headers['Location']
            print ("jar 1 : "+ str(jar))
            print ("redirect 1 : "+redirect_URL1)
            r2 = session.get(redirect_URL1, allow_redirects=False, cookies=jar)
            if r2.status_code == 302: # expected here
                jar2 = r2.cookies
                redirect_URL2 = r2.headers['Location']
                print ("jar 1 : "+ str(jar2))
                print ("redirect 2 : "+redirect_URL2)
                r = session.get(redirect_URL2, allow_redirects=False, cookies=jar2)
                jar.update(jar2)
        print (jar)
        r.raise_for_status()
    except requests.exceptions.HTTPError as errh:
        err= "ERREUR: Http Error: %s" % errh
    except requests.exceptions.ConnectionError as errc:
        err="ERREUR: Error Connecting: %s" % errc
    except requests.exceptions.Timeout as errt:
        err="ERREUR: Timeout Error: %s " % errt
    except requests.exceptions.RequestException as err:
        err="ERREUR: OOps: Something Else : %s" % err
    if err!="":
        print (err)
        return ()

    headers=r.headers
    content=r.content
    mode = ""
    nbnotices = 0
    sessioncookie = ""
    try:
        mode = headers['X-PSI-Context']
    except:
        print ('ERREUR : X-PSI-Context non défini')
        return ()
    try:
        nbnotices = int(headers['X-SDI-Selected'])
    except:
        print ('ERREUR : X-SDI-Selected non défini')
        return ()
    try:
        sessioncookie = headers['Set-Cookie']
    except:
        print ('ERREUR : Set-Cookie non défini')
        return ()
    print ("Nombre de notices : {}".format (nbnotices))
    print ("Mode : "+mode)
    print ("Cookie : "+sessioncookie)

    while (i < j):
        time.sleep(delai)
        print (i)
        base_url="http://www.sudoc.abes.fr/xslt"
        base_params="//DB=2.1/SET="+setparam+'/TTL='+ttlparam+"/NXT?"
        params="FRST="+str(i)
        session.cookies = jar
        url=base_url+base_params+params
        print (url)
        i = i+10
        err=""
        try:
            r = session.get(url,timeout=(7, 27))
            r.raise_for_status()
        except requests.exceptions.HTTPError as errh:
            err= "ERREUR: Http Error: %s" % errh
        except requests.exceptions.ConnectionError as errc:
            err="ERREUR: Error Connecting: %s" % errc
        except requests.exceptions.Timeout as errt:
            err="ERREUR: Timeout Error: %s " % errt
        except requests.exceptions.RequestException as err:
            err="ERREUR: OOps: Something Else : %s" % err
        if err!="":
            print (err)
            return ()
        headers=r.headers
        content=r.content
        # print (content)
        soup = BeautifulSoup(content, 'html.parser')
        tab1 = soup.select('div.tabbar span.tab1')
        if len(tab1) ==0:
            print ("PAS DE REPONSE")
        else:
            active_tab = tab1[0].get_text()
            if active_tab =="Liste des résultats":
                print ("LISTE")
                ppn=""
                titre=""
                date=""
                auteur=""
                editeur=""
                description=""
                tdresults = soup.select('td.result')
                nbres="0"
                for tdresult in tdresults:
                    temp=re.search(r"(\d+)\s+résultat",tdresult.get_text())
                    if temp is not None:
                        nbres = temp.group(1)
# EXEMPLE
#<td class="rec_title">
#<input type="hidden" name="ppn4" value="000971308">
#<div><a class="link_gen" href="SHW?FRST=4">Le marketing direct en France [Texte imprimé] : pratique du marketing direct, vente par correspondance, vente par téléphone, vente à domicile</a></div>
#<div>Manuel, Bruno (1941-....) / 2e édition / Dalloz / 1985</div>
#</td>

                tds = soup.select('table[summary="short title presentation"] tr td.rec_title')
                for item in tds:
                    result={}
                    result['ppn']=item.input['value'].replace("\r","").replace("\n","")
                    result['titre']=item.div.a.get_text().replace("\r","").replace("\n","")
                    result['description']=item.find_all('div')[1].get_text().replace("\r","").replace("\n","")
                    results.append(result)
                # print (results)
            elif active_tab =="Notice détaillée":
# Normalement la recherche par RCR ne devrait pas renvoyer une notice détaillée
                print ("ERREUR : Notice détaillée")
                exit ()
            else :
                print ("ERREUR : Anomalie")
                exit ()
# ecriture d'un fichier par requete

    return (results)


def get_metadata ():
    session = requests.Session()
    session.headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8','Referer': 'http://www.sudoc.abes.fr/xslt/DB=2.1/SET=1/TTL=21//CMD?ACT=SRCHA&IKT=1016&SRT=RLV&TRM=rbc+061522101&COOKIE=U10178,Klecteurweb,I250,B341720009+,SY,NLECTEUR+WEBOPC,D2.1,Ebc3613e0-1,A,H,R176.159.79.166,FY' }
    base_url="http://www.sudoc.abes.fr/xslt"
    base_params="//DB=2.1/SET="+setparam+"/TTL="+ttlparam+"/CMD?"
    params="ACT=SRCHA&IKT=1016&SRT=RLV&TRM=rbc+"+rcr
    url=base_url+base_params+params
    print ("URL : "+url)
    err=""
    try:
        r1 = session.get(url,timeout=(7, 27),allow_redirects=False)
        if r1.status_code == 302: # expected here
            jar = r1.cookies
            redirect_URL1 = r1.headers['Location']
            print ("jar 1 : "+ str(jar))
            print ("redirect 1 : "+redirect_URL1)
            r2 = session.get(redirect_URL1, allow_redirects=False, cookies=jar)
            if r2.status_code == 302: # expected here
                jar2 = r2.cookies
                redirect_URL2 = r2.headers['Location']
                print ("jar 1 : "+ str(jar2))
                print ("redirect 2 : "+redirect_URL2)
                r = session.get(redirect_URL2, allow_redirects=False, cookies=jar2)
                jar.update(jar2)
        print (jar)
        r.raise_for_status()
    except requests.exceptions.HTTPError as errh:
        err= "ERREUR: Http Error: %s" % errh
    except requests.exceptions.ConnectionError as errc:
        err="ERREUR: Error Connecting: %s" % errc
    except requests.exceptions.Timeout as errt:
        err="ERREUR: Timeout Error: %s " % errt
    except requests.exceptions.RequestException as err:
        err="ERREUR: OOps: Something Else : %s" % err
    if err!="":
        print (err)
        return ()

    headers=r.headers
    content=r.content
    mode = ""
    nbnotices = 0
    sessioncookie = ""
    try:
        mode = headers['X-PSI-Context']
    except:
        print ('ERREUR : X-PSI-Context non défini')
        return ()
    try:
        nbnotices = int(headers['X-SDI-Selected'])
    except:
        print ('ERREUR : X-SDI-Selected non défini')
        return ()
    try:
        sessioncookie = headers['Set-Cookie']
    except:
        print ('ERREUR : Set-Cookie non défini')
        return ()
    print ("Nombre de notices : {}".format (nbnotices))
    print ("Mode : "+mode)
    print ("Cookie : "+sessioncookie)
    return (nbnotices)

def main():
    global final
    global rcr
    if len(sys.argv) < 2 :
        print ("ERREUR : ARGUMENT OBLIGATOIRE : RCR à utiliser")
        print ("ERREUR : ARGUMENTS FACULTATIFS : Numéro de l'itération à relancer")
        exit ()
    rcr=sys.argv[1]
    if rcr in liste_rcr.keys():
        print ("RCR accepté")
    else :
        print ("RCR inconnu")
        exit ()

    if len(sys.argv) == 3 :
        new_iter = int(sys.argv[2])
        print ("Relance de l'itération "+str(new_iter))
    

    base_destfile="data"+rcr
    nbnotices=get_metadata()
    destfile=destdossier+"/"+"metadata"+rcr+".tsv"
    with open(destfile, 'w') as sortie:
        writer = csv.writer(sortie, dialect='excel-tab')
        writer.writerow (["Date extraction","14/03/2019"])
        writer.writerow (["Bibliothèque",liste_rcr[rcr]['nom']])
        writer.writerow (["RCR",rcr])
        writer.writerow (["Perimetre",liste_rcr[rcr]['perimetre']])
        writer.writerow (["Dans aleph",liste_rcr[rcr]['aleph']])
        writer.writerow (["Nb notices Sudoc",nbnotices])

    if final == 0:
        final=nbnotices
    i=debut
    iter=1
    while i < final:
        if new_iter == 0 :
            print ("ITERATION "+str(iter) + " : i="+str(i) )
            data=recherche_sudoc (i,i+nbnoticesparfichier)
            destfile=destdossier+"/"+base_destfile+"_{0:0>6}".format(i)+"-{0:0>6}".format(i+nbnoticesparfichier)+".tsv"
            with open(destfile, 'w') as sortie:
                writer = csv.writer(sortie, dialect='excel-tab')
                for result in data:
                    writer.writerow ([result['ppn'], result['titre'],result['description']])
            iter = iter+1
            i=i+nbnoticesparfichier
            time.sleep(delailong)
        else :
            if new_iter == iter :
                
                print ("ITERATION "+str(iter) + " : i="+str(i) )
                data=recherche_sudoc (i,i+nbnoticesparfichier)
                destfile=destdossier+"/"+base_destfile+"_{0:0>6}".format(i)+"-{0:0>6}".format(i+nbnoticesparfichier)+".tsv"
                with open(destfile, 'w') as sortie:
                    writer = csv.writer(sortie, dialect='excel-tab')
                    for result in data:
                        writer.writerow ([result['ppn'], result['titre'],result['description']])
                iter = iter+1
                i=i+nbnoticesparfichier
                time.sleep(delailong)
            else : 
                iter = iter+1
                i=i+nbnoticesparfichier
            
        
            

if __name__ == "__main__":

    main()
