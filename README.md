Récupération de toutes les notices d'une liste de RCR par scraping du Sudoc

0. Créer un dossier "resultat"
2. Lancer ./recupere_donnees.sh : récupération de fichiers de données (par lot de 1000 notices) et de métadonnées (pour chaque RCR)
3. Concaténer les fichiers par RCR et supprimer les fichiers de données téléchargés : ./concat_result.sh + RCR
4. En cas de problème (un fichier de données vide ou incomplet), relancer la récupération d'un lot de données : python3 cherche_sudoc.py [RCR] [iteration] (pour identifier l'itération : rajouter 1 au numéro du fichier problématique. Ex : pour data060882103_016001-017001.tsv ->  python3 cherche_sudoc.py 060882103 17 )
